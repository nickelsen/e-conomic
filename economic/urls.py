from django.conf.urls import url

from . import views

app_name = 'economic'

urlpatterns = [
    url(r'^$', views.TimeRegistrationList.as_view(), name='index'),
    url(r'^new$', views.TimeRegistrationCreate.as_view(), name='timeregistration_new'),
    url(r'^edit/(?P<pk>\d+)$', views.TimeRegistrationUpdate.as_view(), name='timeregistration_edit'),
]