# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-03-13 13:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('economic', '0002_auto_20160313_1347'),
    ]

    operations = [
        migrations.AlterField(
            model_name='timeregistration',
            name='billed',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
