from django.contrib import admin

from .models import Client, Project, TimeRegistration

admin.site.register([Client, Project, TimeRegistration])