from django.utils import timezone

from django.test import TestCase
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
#from selenium.webdriver.firefox.webdriver import WebDriver
from selenium import webdriver
from pyvirtualdisplay import Display

from .models import *

class TimeRegistrationTests(TestCase):
    def test_time_registration_can_be_created(self):
        client = Client.objects.create(user_id = 1, name="test-client")
        project = Project.objects.create(client=client, name="test-project")

        TimeRegistration.objects.create(id=1, project=project, time=12, note="time registration note", performed=timezone.now())

        self.assertEqual("time registration note", TimeRegistration.objects.get(id=1).note)

    def test_index_shows_overview(self):
        User.objects.create_user(username="test", password="test", id=1)
        client = Client.objects.create(user_id = 1, name="test-client")
        project = Project.objects.create(client=client, name="test-project")

        TimeRegistration.objects.create(id=1, project=project, time=12, note="time registration note", performed=timezone.now())
        TimeRegistration.objects.create(id=2, project=project, time=12, note="time registration note", performed=timezone.now())

        print(self.client.login(username="test", password="test"))
        response = self.client.get('/economic/')

        self.assertContains(response, "test-client :: test-project")
        self.assertContains(response, "timeregistration/2")
        self.assertContains(response, "timeregistration/1")

class IntegrationTest(StaticLiveServerTestCase):
    fixtures = ['initial-data.json']

    @classmethod
    def setUpClass(self):
        super(IntegrationTest, self).setUpClass()
        self.display = Display(visible=0, size=(1024, 768))
        self.display.start()
        self.selenium = webdriver.Firefox()

    @classmethod
    def tearDownClass(self):
        self.selenium.quit()
        self.display.stop()
        super(IntegrationTest, self).tearDownClass()

    def test_login(self):
        # When a user provides valid credentials
        self.selenium.get('%s%s' % (self.live_server_url, '/admin/login/'))
        username_input = self.selenium.find_element_by_name("username")
        username_input.send_keys('admin')
        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys('admin01ADF')
        self.selenium.find_element_by_xpath('//input[@value="Log in"]').click()

        # Then the user is logged in
        self.assertIsNotNone(self.selenium.find_element_by_id("user-tools"))

        # And the user can make time registrations
        self.selenium.get('%s%s' % (self.live_server_url, '/economic/'))
        self.selenium.find_element_by_class_name("btn-primary").click()
        self.assertIsNotNone(self.selenium.find_element_by_xpath('//input[@value="Submit"]'))