from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.contrib.auth.models import User

from django.db import models

class Client(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name

class Project(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return "%s :: %s" % (self.client, self.name)

class TimeRegistration(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    time = models.IntegerField()
    note = models.TextField()
    performed = models.DateTimeField()
    billed = models.DateTimeField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    changed = models.DateTimeField(auto_now=True)

    def get_absolute_url(self):
        return reverse('economic:timeregistration_edit', kwargs={'pk': self.pk})