from __future__ import unicode_literals

from django.apps import AppConfig


class EconomicConfig(AppConfig):
    name = 'economic'
