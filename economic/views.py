from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView,ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy

from .models import *

@method_decorator(login_required, name='dispatch')
class TimeRegistrationList(ListView):
    model = TimeRegistration

    def get_queryset(self):
        return TimeRegistration.objects.order_by('-created')

@method_decorator(login_required, name='dispatch')
class TimeRegistrationCreate(CreateView):
    model = TimeRegistration
    fields = ['project', 'time', 'note', 'performed', 'billed']
    success_url = reverse_lazy('economic:index')

@method_decorator(login_required, name='dispatch')
class TimeRegistrationUpdate(UpdateView):
    model = TimeRegistration
    fields = ['project', 'time', 'note', 'performed', 'billed']
    success_url = reverse_lazy('economic:index')