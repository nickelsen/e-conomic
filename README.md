# Economic homework

# User stories
- As a freelancer I want to be able to register the time I spend on my projects, so that I can create correct invoices for my customers.
- As a freelancer I want to be able to get an overview of my time registrations, so that I can create correct invoices for my customers.

# Solution
Vagrantbox with everything needed to run the app.

App implemented in Django on top of Postgres. Django is an MVC framework for python that provides a lot of the boilerplate needed to run an app; ORM, request and response routing, authentication, templating, test framework, schema migrations, etc.

The app uses the default authentication system, which is why it fails if you go to `/economic/` without an authenticated session. Along the same lines, the objects have a foreign reference to a user, but I did not have time to make sure that e.g. views only show the objects of the logged in user, which of course would be needed in a production system.

Protected by 2 unit tests and 1 integration test (Selenium)

# Get it up and running
- make sure you have virtualbox and vagrant installed
- run `vagrant up`
- run `vagrant ssh`
- run `python /opt/code/manage.py createsuperuser --username admin --email admin@mailinator.com` with password admin01ADF
- run `python /opt/code/manage.py runserver 0.0.0.0:8000`
- access the system on the virtualbox, e.g. http://172.28.128.3:8000/admin
- sign in to the system using the superuser
- create a client and a project in the admin interface
- go to http://172.28.128.3:8000/economic/ to see and make time registrations
 
# Run tests
- ssh into the box by `vagrant ssh`
- run `python /opt/code/manage.py test`
