#!/bin/bash

apt-get update
apt-get install -y python-pip postgresql-9.3 python-psycopg2 firefox xvfb

pip install -r /opt/code/requirements.txt

sudo su postgres -c "echo \"ALTER USER postgres WITH PASSWORD 'poc';\" | psql"
sudo su postgres -c "echo \"CREATE DATABASE pocdb;\" | psql"

python /opt/code/manage.py migrate
